package com.fixo.advertiser;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;


/**
 * Created by FIXO on 08/08/2018.
 */

public class AreasInNairobi extends AppCompatActivity {

    private TextView rongai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.areas_in_nairobi);
        rongai = findViewById(R.id.rongai);

        rongai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(com.fixo.advertiser.AreasInNairobi.this,RecyclerViewMainActivity.class);
                startActivity(intent);

            }
        });
    }

    }
