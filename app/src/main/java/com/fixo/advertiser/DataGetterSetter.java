package com.fixo.advertiser;

/**
 * Created by FIXO on 11/08/2018.
 */

public class DataGetterSetter {

    private String image1,image2,text;

    public DataGetterSetter(String image1, String image2, String text) {
        this.image1 = image1;
        this.image2 = image2;
        this.text = text;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
