package com.fixo.advertiser;

/**
 * Created by FIXO on 13/08/2018.
 */

public class DataGetterSetter2 {

    String image1, image2,image4;

    public DataGetterSetter2(String image1, String image2, String image4) {
        this.image1 = image1;
        this.image2 = image2;
        this.image4 = image4;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }
}
