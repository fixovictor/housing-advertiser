package com.fixo.advertiser;

/**
 * Created by FIXO on 18/06/2018.
 */

public class HouseGetterSetter {


    private String name, location, rent, bedrooms, description, main_picure, kitchen, mainBedroom, bedroomTwo, bedroomThree, livingRoom, diningRoom, swimmingPool;

    public HouseGetterSetter(String name, String location, String rent, String bedrooms, String description, String main_picure, String kitchen, String mainBedroom, String bedroomTwo, String bedroomThree, String livingRoom, String diningRoom, String swimmingPool) {
        this.name = name;
        this.location = location;
        this.rent = rent;
        this.bedrooms = bedrooms;
        this.description = description;
        this.main_picure = main_picure;
        this.kitchen = kitchen;
        this.mainBedroom = mainBedroom;
        this.bedroomTwo = bedroomTwo;
        this.bedroomThree = bedroomThree;
        this.livingRoom = livingRoom;
        this.diningRoom = diningRoom;
        this.swimmingPool = swimmingPool;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRent() {
        return rent;
    }

    public void setRent(String rent) {
        this.rent = rent;
    }

    public String getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(String bedrooms) {
        this.bedrooms = bedrooms;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMain_picure() {
        return main_picure;
    }

    public void setMain_picure(String main_picure) {
        this.main_picure = main_picure;
    }

    public String getKitchen() {
        return kitchen;
    }

    public void setKitchen(String kitchen) {
        this.kitchen = kitchen;
    }

    public String getMainBedroom() {
        return mainBedroom;
    }

    public void setMainBedroom(String mainBedroom) {
        this.mainBedroom = mainBedroom;
    }

    public String getBedroomTwo() {
        return bedroomTwo;
    }

    public void setBedroomTwo(String bedroomTwo) {
        this.bedroomTwo = bedroomTwo;
    }

    public String getBedroomThree() {
        return bedroomThree;
    }

    public void setBedroomThree(String bedroomThree) {
        this.bedroomThree = bedroomThree;
    }

    public String getLivingRoom() {
        return livingRoom;
    }

    public void setLivingRoom(String livingRoom) {
        this.livingRoom = livingRoom;
    }

    public String getDiningRoom() {
        return diningRoom;
    }

    public void setDiningRoom(String diningRoom) {
        this.diningRoom = diningRoom;
    }

    public String getSwimmingPool() {
        return swimmingPool;
    }

    public void setSwimmingPool(String swimmingPool) {
        this.swimmingPool = swimmingPool;
    }
}