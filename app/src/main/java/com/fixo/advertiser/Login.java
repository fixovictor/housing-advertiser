package com.fixo.advertiser;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by FIXO on 24/07/2018.
 */

public class Login extends AppCompatActivity {

    private Button userLoginButton,ownerLoginButton;
    private Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_page);

        userLoginButton=findViewById(R.id.normalUserBotton);
        ownerLoginButton=findViewById(R.id.ownerButton);
        register=findViewById(R.id.registrationLink);

        userLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(com.fixo.advertiser.Login.this,UserLogin.class);
                startActivity(intent);
            }
        });

        ownerLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(com.fixo.advertiser.Login.this,OwnerLogin.class);
                startActivity(intent);
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(com.fixo.advertiser.Login.this,UserRegistration.class);
                startActivity(intent);
            }
        });

    }

}
