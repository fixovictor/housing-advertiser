package com.fixo.advertiser;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by FIXO on 23/07/2018.
 */

public class LoginProcess extends AppCompatActivity{
    EditText password,name;
    Button login;
    AlertDialog alertDialog;
    String username, userPassword;

    private  String address="http://192.168.44.112/android/login.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_process);
        password=findViewById(R.id.password);
        name=findViewById(R.id.name);
        login=findViewById(R.id.loginButton);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 username=name.getText().toString();
                 userPassword=password.getText().toString();
                DoinBackGround task=new DoinBackGround();
                task.execute(username,userPassword);
            }
        });

    }

@SuppressLint("StaticFieldLeak")
private class DoinBackGround extends AsyncTask<String,Void,String>{

    @Override
    protected String doInBackground(String... voids) {
        try {
            URL url=new URL(address);
            HttpURLConnection httpURLConnection=(HttpURLConnection)url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);

            //opening an output stream
            OutputStream outputStream=httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
            String data_to_post= URLEncoder.encode("name","UTF-8")+"="+URLEncoder.encode(username,"UTF-8")+"&"
                    + URLEncoder.encode("password","UTF-8")+"="+URLEncoder.encode(userPassword,"UTF-8");
            bufferedWriter.write(data_to_post);

            //closing open objects
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();

            Log.i("Data","Message"+data_to_post);

            InputStream inputStream=httpURLConnection.getInputStream();
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
            String line;
            String result="";

            while((line=bufferedReader.readLine())!=null)
            {
                result+=line;
            }

            //closing open connections
            bufferedReader.close();
            inputStream.close();
            httpURLConnection.disconnect();
            return result;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        alertDialog=new AlertDialog.Builder(com.fixo.advertiser.LoginProcess.this).create();
        alertDialog.setTitle("Login status");

    }

    @Override
    protected void onPostExecute(String result) {

        alertDialog.setMessage(result);
        alertDialog.show();

    }
}



}
