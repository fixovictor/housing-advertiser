package com.fixo.advertiser;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private String url="http://10.0.3.2/android/goods.php";

    private DrawerLayout drawerLayout;
   // private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        drawerLayout = findViewById(R.id.countiesDrawerLayout);


        Toolbar toolbar=findViewById(R.id.countiesToolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        //setting the toolbar as the app bar
        setSupportActionBar(toolbar);
        ActionBar actionBar=getSupportActionBar();
        assert actionBar != null;
        //enabling the App Bar's home button by calling setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true);
        //Making the home button to use the menu icon
        actionBar.setHomeAsUpIndicator(R.drawable.menu_icon);

        NavigationView navigationView = findViewById(R.id.countiesNavigationView);

        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        // close drawer when item is tapped
                        drawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here


                            Fragment fragment=null;
                            switch (menuItem.getItemId())
                            {
                                case R.id.nairobi:
                                    fragment=new Fragement1();
                                    break;
                                case R.id.kisumu:
                                    fragment=new Fragement1();
                                    break;
                                default:
                                    break;
                            }
                            if(fragment!=null)
                            {
                                FragmentManager fragmentManager=getSupportFragmentManager();
                                fragmentManager.beginTransaction().replace(R.id.countiesFrameLayout,fragment).commit();
                            }

                        return true;
                    }
                });


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                //GravityCompat.START ensures that the nav drawer open animation
                //behave properly for both right-to-left and left-to-right layouts
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



    public void backGroundTasks()
    {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void,Void,Void> Task=new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {

                JsonArrayRequest jsonArrayRequest=new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        if(response.length()>0)
                        {

                            for(int i=0;i<response.length();i++)
                            {
                                try {
                                    JSONObject jsonObject=response.getJSONObject(i);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Volley Error",": "+error.getMessage());

                    }
                });

                RequestQueue requestQueue;
                return null;
            }
        };
    }
}
