package com.fixo.advertiser;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

/**
 * Created by FIXO on 08/08/2018.
 */

public class MoreDetails extends AppCompatActivity{

    private ImageView mainHouse,kitchentImage,bedroomOne,bedroomTwo,bedroomThree,diningRoom,swimmingPool,livingRoom;
    private TextView houseName, houseLocation,houseDescription,bedrooms;
    private  String mainHouseString,kitchenString,bedroomOneString,bedroomTwoString,bedroomThreeString,diningRoomString,swimmingPoolString,livingRoomString;
    private String houseNameString, houseLocationString,houseDescriptionString,bedroomsString;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.more_details);
        mainHouse = findViewById(R.id.mainHouse);
        kitchentImage = findViewById(R.id.kitchen);
        bedroomOne = findViewById(R.id.mainBedroom);
        bedroomTwo = findViewById(R.id.bedroomTwo);
        bedroomThree = findViewById(R.id.bedroomThree);
        diningRoom = findViewById(R.id.diningRoom);
        swimmingPool = findViewById(R.id.swimmingPool);
        livingRoom = findViewById(R.id.livingRoom);
        houseName = findViewById(R.id.houseName);
        houseLocation = findViewById(R.id.location);
        houseDescription = findViewById(R.id.description);
        bedrooms=findViewById(R.id.bedrooms);

        mainHouseString = getIntent().getStringExtra("mainHouse");
        kitchenString = getIntent().getStringExtra("kitchen");
        bedroomOneString = getIntent().getStringExtra("mainBedroom");
        bedroomTwoString = getIntent().getStringExtra("bedroomTwo");
        bedroomThreeString = getIntent().getStringExtra("bedroomThree");
        diningRoomString = getIntent().getStringExtra("diningRoom");
        swimmingPoolString = getIntent().getStringExtra("swimmingPool");
        livingRoomString = getIntent().getStringExtra("livingRoom");
        houseNameString = getIntent().getStringExtra("name");
        houseLocationString = getIntent().getStringExtra("location");
        houseDescriptionString = getIntent().getStringExtra("description");
        bedroomsString=getIntent().getStringExtra("bedrooms");
        houseName.setText(houseNameString);
        houseDescription.setText(houseDescriptionString);
        bedrooms.setText(bedroomsString);
        houseLocation.setText(houseLocationString);

        Glide.with(this).load(mainHouseString).into(mainHouse);
        Glide.with(this).load(kitchenString).into(kitchentImage);
        Glide.with(this).load(bedroomOneString).into(bedroomOne);
        Glide.with(this).load(bedroomOneString).into(bedroomOne);
        Glide.with(this).load(bedroomOneString).into(bedroomOne);
        Glide.with(this).load(swimmingPoolString).into(swimmingPool);
        Glide.with(this).load(bedroomTwoString).into(bedroomTwo);
        Glide.with(this).load(bedroomThreeString).into(bedroomThree);
        Glide.with(this).load(diningRoomString).into(diningRoom);
        Glide.with(this).load(livingRoomString).into(livingRoom);
    }



}
