package com.fixo.advertiser;

/**
 * Created by FIXO on 28/06/2018.
 */

public class Movie {

    String heading, image, description, kitchen, bedroomOne, bedroomTwo, bedroomThree, livingRoom, dining, swimmingPool;

    public Movie(String heading, String image, String description, String kitchen, String bedroomOne, String bedroomTwo, String bedroomThree, String livingRoom, String dining, String swimmingPool) {
        this.heading = heading;
        this.image = image;
        this.description = description;
        this.kitchen = kitchen;
        this.bedroomOne = bedroomOne;
        this.bedroomTwo = bedroomTwo;
        this.bedroomThree = bedroomThree;
        this.livingRoom = livingRoom;
        this.dining = dining;
        this.swimmingPool = swimmingPool;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKitchen() {
        return kitchen;
    }

    public void setKitchen(String kitchen) {
        this.kitchen = kitchen;
    }

    public String getBedroomOne() {
        return bedroomOne;
    }

    public void setBedroomOne(String bedroomOne) {
        this.bedroomOne = bedroomOne;
    }

    public String getBedroomTwo() {
        return bedroomTwo;
    }

    public void setBedroomTwo(String bedroomTwo) {
        this.bedroomTwo = bedroomTwo;
    }

    public String getBedroomThree() {
        return bedroomThree;
    }

    public void setBedroomThree(String bedroomThree) {
        this.bedroomThree = bedroomThree;
    }

    public String getLivingRoom() {
        return livingRoom;
    }

    public void setLivingRoom(String livingRoom) {
        this.livingRoom = livingRoom;
    }

    public String getDining() {
        return dining;
    }

    public void setDining(String dining) {
        this.dining = dining;
    }

    public String getSwimmingPool() {
        return swimmingPool;
    }

    public void setSwimmingPool(String swimmingPool) {
        this.swimmingPool = swimmingPool;
    }
}



