package com.fixo.advertiser;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by FIXO on 24/07/2018.
 */

public class OwnerLogin extends AppCompatActivity {
   private EditText username, password;
   private Button loginButton,register;
   private AlertDialog alertDialog;
   private String name,userPassword;
    private  String address="http://192.168.44.112/android/login.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.owner_login);
        username=findViewById(R.id.ownerName);
        password=findViewById(R.id.ownerPassword);
        loginButton=findViewById(R.id.ownerLoginButton);
        register=findViewById(R.id.ownerRegistrationButton);


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 name=username.getText().toString();
                 userPassword=password.getText().toString();
                DoInBackGround doinBackGround=new DoInBackGround();
                doinBackGround.execute(name,userPassword);
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(com.fixo.advertiser.OwnerLogin.this,UserRegistration.class);
                startActivity(intent);

            }
        });

    }



    @SuppressLint("StaticFieldLeak")
    private class DoInBackGround extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... voids) {
            try {
                URL url=new URL(address);
                HttpURLConnection httpURLConnection=(HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);

                //opening an output stream
                OutputStream outputStream=httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data_to_post= URLEncoder.encode("name","UTF-8")+"="+URLEncoder.encode(name,"UTF-8")+"&"
                        + URLEncoder.encode("password","UTF-8")+"="+URLEncoder.encode(userPassword,"UTF-8");
                bufferedWriter.write(data_to_post);

                //closing open objects
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                Log.i("Data","Message"+data_to_post);

                InputStream inputStream=httpURLConnection.getInputStream();
                BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String line;
                String result="";

                while((line=bufferedReader.readLine())!=null)
                {
                    result+=line;
                }

                //closing open connections
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            alertDialog=new AlertDialog.Builder(com.fixo.advertiser.OwnerLogin.this).create();
            alertDialog.setTitle("Login status");

        }

        @Override
        protected void onPostExecute(String result) {
            String value=result;
            alertDialog.setMessage(result);

            try {
                if(value.contains("Login Successful"))
                {
                    username.setText("");
                    password.setText("");
                    Intent intent=new Intent(com.fixo.advertiser.OwnerLogin.this,TrialImageUpload.class);
                    startActivity(intent);
                }
                else
                {
                    alertDialog.show();
                }

            }catch (NullPointerException e)
            {
                Toast.makeText(com.fixo.advertiser.OwnerLogin.this,"No network connection",Toast.LENGTH_LONG).show();
            }
        }
    }

}
