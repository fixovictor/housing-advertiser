package com.fixo.advertiser;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by FIXO on 21/06/2018.
 */

public class RecyclerViewAdapter  extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private Context context;
    private List<HouseGetterSetter> houseList;

    public RecyclerViewAdapter(Context context, List<HouseGetterSetter> houseList) {
        this.context = context;
        this.houseList = houseList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item,parent,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        holder.shopName.setText(houseList.get(position).getShopName());
//        Picasso.with(context).load(houseList.get(position).getImage()).into(holder.imageView);

        final HouseGetterSetter houseGetterSetter=houseList.get(position);
        holder.houseName.setText(houseGetterSetter.getName());
        holder.description.setText(houseGetterSetter.getDescription());
        holder.rent.setText(houseGetterSetter.getRent());

       Glide.with(context).load(houseList.get(position).getMain_picure()).into(holder.imageView);


       holder.cardView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

              String mainHouseImage=houseGetterSetter.getMain_picure();
              String kitchenImage=houseGetterSetter.getKitchen();
              String mainBedroom=houseGetterSetter.getMainBedroom();
              String bedroomTwo=houseGetterSetter.getBedroomTwo();
              String bedroomThree=houseGetterSetter.getBedroomThree();
              String livingRoom=houseGetterSetter.getLivingRoom();
              String dininroom=houseGetterSetter.getLivingRoom();
              String swimmingPool=houseGetterSetter.getSwimmingPool();
              String houseName=houseGetterSetter.getName();
              String description=houseGetterSetter.getDescription();
              String location=houseGetterSetter.getLocation();
              String bedrooms=houseGetterSetter.getBedrooms();

              Intent intent=new Intent(context,MoreDetails.class);

              intent.putExtra("mainHouse",mainHouseImage);
               intent.putExtra("kitchen",kitchenImage);
               intent.putExtra("mainBedroom",mainBedroom);
               intent.putExtra("bedroomTwo",bedroomTwo);
               intent.putExtra("bedroomThree",bedroomThree);
               intent.putExtra("livingRoom",livingRoom);
               intent.putExtra("diningRoom",dininroom);
               intent.putExtra("swimmingPool",swimmingPool);
               intent.putExtra("description",description);
               intent.putExtra("name",houseName);
               intent.putExtra("location",location);
               intent.putExtra("bedrooms",bedrooms);
               context.startActivity(intent);
           }
       });

    }

    @Override
    public int getItemCount() {
        return houseList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView houseName,description,rent;
        public ImageView imageView;
        public CardView cardView;

        public ViewHolder(View itemView)
        {
            super(itemView);
            houseName=itemView.findViewById(R.id.heading);
            description=itemView.findViewById(R.id.description);
            imageView=itemView.findViewById(R.id.imageView);
            cardView=itemView.findViewById(R.id.houseListCardView);
            rent=itemView.findViewById(R.id.rent);

        }
    }
}
