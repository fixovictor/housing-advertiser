package com.fixo.advertiser;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ProgressBar;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FIXO on 21/06/2018.
 */

public class RecyclerViewMainActivity extends AppCompatActivity{

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private List<HouseGetterSetter> movieList;
    private RecyclerView.Adapter adapter;
    //private String url="http://10.0.3.2/android/advertiser.php";
    private String url="http://192.168.44.112/android/trial4.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recycler_view_layout);
        recyclerView=findViewById(R.id.recyclerView);
        movieList=new ArrayList<>();
        adapter=new RecyclerViewAdapter(getApplicationContext(),movieList);
        linearLayoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        getData();
    }


    public void getData() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        //Volley cannot be inside AsyncTask because  it initiates background thread(s) on it own.
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject jsonObject = response.getJSONObject(i);
                       HouseGetterSetter movie=new HouseGetterSetter(jsonObject.optString("name"),jsonObject.optString("location"),jsonObject.optString("rent"),jsonObject.optString("bedrooms"),jsonObject.optString("description"),jsonObject.optString("main_picture"),jsonObject.optString("ktchen"),jsonObject.optString("main_bedroom"),jsonObject.optString("bedroom_two"),jsonObject.optString("bedroom_three"),jsonObject.optString("living_room"),jsonObject.optString("dining_room"),jsonObject.optString("swimming_pool"));
                       movie.setName(jsonObject.optString("name"));
                       movie.setLocation(jsonObject.optString("location"));
                       movie.setDescription(jsonObject.optString("description"));
                       movie.setKitchen(jsonObject.optString("ktchen"));
                       movie.setMainBedroom(jsonObject.optString("main_bedroom"));
                       movie.setBedroomTwo(jsonObject.optString("bedroom_two"));
                       movie.setBedroomThree(jsonObject.optString("bedroom_three"));
                       movie.setLivingRoom(jsonObject.optString("living_room"));
                       movie.setRent(jsonObject.optString("rent"));
                       movie.setBedrooms(jsonObject.optString("bedrooms"));
                       movie.setDiningRoom(jsonObject.optString("dining_room"));
                       movie.setSwimmingPool(jsonObject.optString("swimming_pool"));
                       movieList.add(movie);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }

                }
                adapter.notifyDataSetChanged();
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", "error: " + error.getMessage());
                progressDialog.dismiss();

            }
        });
        RequestQueue requestQueue=Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }


}




