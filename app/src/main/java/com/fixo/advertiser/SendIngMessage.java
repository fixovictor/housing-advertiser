package com.fixo.advertiser;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by FIXO on 03/08/2018.
 */

public class SendIngMessage extends AppCompatActivity {

    Button send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sms);
        ///
        /// send=findViewById(R.id.sms);


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT)
//                {
//                    int number=327382783;
//                    String defaultPackage= Telephony.Sms.getDefaultSmsPackage(com.fixo.advertiser.SendIngMessage.this);
//                    Intent sendMessageIntent=new Intent(Intent.ACTION_SENDTO);
//                    sendMessageIntent.setType("text/plain");
//
//                    sendMessageIntent.putExtra(android.content.Intent.EXTRA_TEXT,"text");
//                    sendMessageIntent.putExtra(android.content.Intent.EXTRA_PHONE_NUMBER,number);
//
//                    if (defaultPackage!=null)
//                    {
//                        sendMessageIntent.setPackage(defaultPackage);
//                    }
//                           try{
//                       com.fixo.advertiser.SendIngMessage.this.startActivity(sendMessageIntent);
//                           }
//                           catch (android.content.ActivityNotFoundException e)
//                           {
//                               Toast.makeText(com.fixo.advertiser.SendIngMessage.this,"No messaging application installed",Toast.LENGTH_LONG).show();
//                           }
//                }
//                else
//                {
//                    Intent sendMessageIntent=new Intent(Intent.ACTION_VIEW);
//                    sendMessageIntent.setType("vnd.android-dir/mms-sms");
//                    sendMessageIntent.putExtra("address","0723456789");
//                    sendMessageIntent.putExtra("sms_body","Type message here");
//                    startActivity(sendMessageIntent);
//                }

                String address="fixoothina@gmail.com'";
               //String[] receipients=address;
                String mesg="Is it working?";
                String subject="trial";
                Intent intent=new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_EMAIL,address);
                intent.putExtra(Intent.EXTRA_SUBJECT,subject);
                intent.putExtra(Intent.EXTRA_TEXT,mesg);
                intent.setType("message/rfc822");
                startActivity(Intent.createChooser(intent,"Choose and email app"));
            }
        });

    }

    }
