package com.fixo.advertiser;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by FIXO on 16/08/2018.
 */

public class TabAdapter extends FragmentStatePagerAdapter {
    String tabaArray[]=new String[]{"Message","Notifications"};
    int numberOfTabs=2;


    public TabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabaArray[position];
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                Fragement1 fragement1=new Fragement1();
                return  fragement1;
            case 1:
                Fragment2 fragment2=new Fragment2();
                return  fragment2;


        }
        return null;
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
