package com.fixo.advertiser;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by FIXO on 11/08/2018.
 */

public class TrialAdapter extends RecyclerView.Adapter<TrialAdapter .ViewHolder> {



        private Context context;
        private List<DataGetterSetter2> list;

        public TrialAdapter (Context context, List<DataGetterSetter2> list) {
            this.context = context;
            this.list = list;
        }

        @NonNull
        @Override
        public TrialAdapter .ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v= LayoutInflater.from(context).inflate(R.layout.card_view, parent,false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

           final DataGetterSetter2 movie=list.get(position);
           // holder.description.setText(movie.getDescription());
//            holder.heading.setText(movie.getText());
//            String address="http://192.168.44.112/android/images/kitchen.jpg2";


            Glide.with(context).load(movie.getImage2()).centerCrop().placeholder(R.drawable.ic_launcher_background).into(holder.imageView);
            Glide.with(context).load(movie.getImage4()).into(holder.imageView2);

//
//            holder.cardView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    String imageView1String=movie.getImage1();
//                    String image2=movie.getImage2();
//                    String title=movie.getText();
//
//                    Intent intent=new Intent(context,TrialMoreDetails.class);
//                    intent.putExtra("image1",imageView1String);
//                    intent.putExtra("image2",image2);
//                    intent.putExtra("title",title);
//                    context.startActivity(intent);
//                }
//            });


        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView heading;
            public ImageView imageView,imageView2;
            public CardView cardView;
            public ViewHolder(View itemView) {
                super(itemView);
               // description=itemView.findViewById(R.id.description);
                heading=itemView.findViewById(R.id.shopName);
                imageView=itemView.findViewById(R.id.imageView);
                imageView2=itemView.findViewById(R.id.imageView2);
                cardView=itemView.findViewById(R.id.cardView);
            }
        }

    }
