package com.fixo.advertiser;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.UUID;

/**
 * Created by FIXO on 12/08/2018.
 */

public class TrialImageUpload extends AppCompatActivity {
    AlertDialog.Builder alertDialog;

    private TextView chooseMainHouse,chooseKitchen,chooseMainBedroom,chooseBedroomTwo;
    private TextView chooseBedroomThree, chooseLivingRoom,chooseDiningRoom,chooseSwimmingPool;

    private TextView mainHouseName,mainBedroomName,kitchenName,bedroomTwoName;
    private TextView bedroomThreeName,livingRoomName,diningRoomName,swimmingPoolName;
    Button upload,choose;
    ImageView imageView,mainHouseImage,kitchenImage,mainBedroomImage,bedroomTwoImage;
    private ImageView bedroomThreeImage,livingRoomImage,dinigRoomImage,swimmingPoolImage;
    private static final int PERMISSI0N_CODE=456;
    private static final int IMAGE_REQUEST_CODE=34;
    private static final int IMAGE_REQUEST_CODE2=40,IMAGE_REQUEST_CODE3=50,IMAGE_REQUEST_CODE4=60,IMAGE_REQUEST_CODE5=70;
    private static final int IMAGE_REQUEST_CODE6=80,IMAGE_REQUEST_CODE7=90,IMAGE_REQUEST_CODE8=100;
    private Uri mainHouseFilePath,mainBedroomFilePath,kitchenFilePath,bedroomTwoFilePath;
    private Uri bedroomThreeFilePath,livingRoomFilePath,diningRoomFilePath,swimmingPoolFilePath;
    private Bitmap mainHouseBitmap,mainBedroomBitmap,kitchenBitmap,bedroomTwoBitmap,bedroomThreeBitmap;
    private Bitmap livingRoomBitmap,diningRoomBitmap,swimmingPoolBitmap;
    EditText locationOfProperty,descriptionOfProperty, numberOfBedrooms,rent;
    String locationString, descriptionString,bedroomString,rentString;

    private String url="http://192.168.44.112/android/trial.php";
    private  String address="http://192.168.44.112/android/login.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_upload);
        mainHouseName=findViewById(R.id.mainHouseName);
        kitchenName=findViewById(R.id.kitchenName);
        mainBedroomName=findViewById(R.id.mainBedroomName);
        bedroomTwoName=findViewById(R.id.bedroomTwoName);
        bedroomThreeName=findViewById(R.id.bedroomThreeName);
        livingRoomName=findViewById(R.id.livingRoomName);
        diningRoomName=findViewById(R.id.diningRoomName);
        swimmingPoolName=findViewById(R.id.swimmingPoolName);
        descriptionOfProperty=findViewById(R.id.descriptionOfProperty);
        locationOfProperty=findViewById(R.id.locationOfProperty);
        numberOfBedrooms=findViewById(R.id.bedrooms);
        rent=findViewById(R.id.rent);


        upload=findViewById(R.id.uploadButton);
        //choose=findViewById(R.id.chooseButton);
       //imageView=findViewById(R.id.imageView);
        mainHouseImage=findViewById(R.id.mainHouse);
        kitchenImage=findViewById(R.id.kitchen);
        mainBedroomImage=findViewById(R.id.mainBedroom);
        bedroomTwoImage=findViewById(R.id.bedroomOne);
        bedroomThreeImage=findViewById(R.id.bedroomThree);
        livingRoomImage=findViewById(R.id.livingRoom);
        dinigRoomImage=findViewById(R.id.diningRoom);
        swimmingPoolImage=findViewById(R.id.swimmingPool);

        chooseMainHouse=findViewById(R.id.chooseMainHouse);
        chooseKitchen=findViewById(R.id.chooseKitchen);
        chooseMainBedroom=findViewById(R.id.chooseMainBedroom);
        chooseBedroomTwo=findViewById(R.id.chooseBedroomOne);
        chooseBedroomThree=findViewById(R.id.chooseBedroomThree);
        chooseLivingRoom=findViewById(R.id.chooseLivingRoom);
        chooseDiningRoom=findViewById(R.id.chooseDiningRoom);
        chooseSwimmingPool=findViewById(R.id.chooseSwimmingPool);


        chooseMainHouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fileChooser();
            }
        });
        chooseKitchen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kitchenFileChooser();
            }
        });

     chooseMainBedroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fileChooser2();
            }
        });
        chooseBedroomTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               bedroomTwoFileChooser();
            }
        });
        chooseBedroomThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bedroomThreeFileChooser();
            }
        });
        chooseLivingRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                livingRoomFileChooser();
            }
        });

        chooseDiningRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                diningRoomFileChooser();
            }
        });
        chooseSwimmingPool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               swimmingPoolFileChooser();
            }
        });

        requestPermission();


        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               try{
                  uploadImages();
                  uploadMainBedroomImage();
                  uploadDiningRoomImage();
                  uploadKitchenImage();
                  uploadBedroomTwoImage();
                  uploadBedroomThreeImage();
                  uploadLivingRoomImage();
                  uploadSwimmingPoolImage();
                   rentString=rent.getText().toString();
                  locationString=locationOfProperty.getText().toString();
                  bedroomString=numberOfBedrooms.getText().toString();
                  descriptionString=descriptionOfProperty.getText().toString();
//                  UploadData uploadData=new UploadData();
//                  uploadData.execute(locationString,bedroomString,descriptionString);
               }catch (NullPointerException e)
               {
                   Toast.makeText(TrialImageUpload.this, "Choose Photos first", Toast.LENGTH_SHORT).show();
               }
            }
        });

    }

    //a method to ask the user to allow to read extenal storage
    public void requestPermission()
    {
        //check if permission is granted

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED)
        {
            return;
        }
        else
        {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},PERMISSI0N_CODE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==PERMISSI0N_CODE)
        {
            if (grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
            {
                Toast.makeText(this, "Permission granted", Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(this, "Permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    //a method to show the file chooser

    public void fileChooser()
    {
        Intent intent=new Intent();
        intent.setType("image/*");  //this will show only image files
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select picture"),IMAGE_REQUEST_CODE);
    }

    public void fileChooser2()
    {
        Intent intent1=new Intent();
        intent1.setType("image/*");  //this will show only image files
        intent1.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent1,"Select picture"),IMAGE_REQUEST_CODE2);
    }

    public void diningRoomFileChooser()
    {
        Intent intent3=new Intent();
        intent3.setType("image/*");  //this will show only image files
        intent3.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent3,"Select picture"),IMAGE_REQUEST_CODE3);
    }


    public void kitchenFileChooser()
    {
        Intent intent4=new Intent();
        intent4.setType("image/*");  //this will show only image files
        intent4.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent4,"Select picture"),IMAGE_REQUEST_CODE4);
    }

    public void bedroomTwoFileChooser()
    {
        Intent intent5=new Intent();
        intent5.setType("image/*");  //this will show only image files
        intent5.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent5,"Select picture"),IMAGE_REQUEST_CODE5);
    }

    public void  bedroomThreeFileChooser()
    {
        Intent intent6=new Intent();
        intent6.setType("image/*");  //this will show only image files
        intent6.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent6,"Select picture"),IMAGE_REQUEST_CODE6);
    }

    public void livingRoomFileChooser()
    {
        Intent intent7=new Intent();
        intent7.setType("image/*");  //this will show only image files
        intent7.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent7,"Select picture"),IMAGE_REQUEST_CODE7);
    }

    public void swimmingPoolFileChooser()
    {
        Intent intent8=new Intent();
        intent8.setType("image/*");  //this will show only image files
        intent8.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent8,"Select picture"),IMAGE_REQUEST_CODE8);
    }


    /// a method to handel file chooser Activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==IMAGE_REQUEST_CODE && resultCode==RESULT_OK && data!=null && data.getData()!=null )
        {
            mainHouseFilePath=data.getData();

            //bitmap for storing the image
            try {
                mainHouseBitmap= MediaStore.Images.Media.getBitmap(getContentResolver(),mainHouseFilePath);
                mainHouseImage.setImageBitmap(mainHouseBitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (requestCode==IMAGE_REQUEST_CODE2 && resultCode==RESULT_OK && data!=null && data.getData()!=null )
        {
            mainBedroomFilePath=data.getData();
            //bitmap for storing the image
            try {

                mainBedroomBitmap= MediaStore.Images.Media.getBitmap(getContentResolver(),mainBedroomFilePath);
                mainBedroomImage.setImageBitmap(mainBedroomBitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (requestCode==IMAGE_REQUEST_CODE3 && resultCode==RESULT_OK && data!=null && data.getData()!=null )
        {
            diningRoomFilePath=data.getData();
            //bitmap for storing the image
            try {

                diningRoomBitmap= MediaStore.Images.Media.getBitmap(getContentResolver(),diningRoomFilePath);
                dinigRoomImage.setImageBitmap(diningRoomBitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (requestCode==IMAGE_REQUEST_CODE4 && resultCode==RESULT_OK && data!=null && data.getData()!=null )
        {
            kitchenFilePath=data.getData();
            //bitmap for storing the image
            try {

                kitchenBitmap= MediaStore.Images.Media.getBitmap(getContentResolver(),kitchenFilePath);
                kitchenImage.setImageBitmap(kitchenBitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (requestCode==IMAGE_REQUEST_CODE5 && resultCode==RESULT_OK && data!=null && data.getData()!=null )
        {
            bedroomTwoFilePath=data.getData();
            //bitmap for storing the image
            try {

                bedroomTwoBitmap= MediaStore.Images.Media.getBitmap(getContentResolver(),bedroomTwoFilePath);
                bedroomTwoImage.setImageBitmap(bedroomTwoBitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (requestCode==IMAGE_REQUEST_CODE6 && resultCode==RESULT_OK && data!=null && data.getData()!=null )
        {
            bedroomThreeFilePath=data.getData();
            //bitmap for storing the image
            try {

                bedroomThreeBitmap= MediaStore.Images.Media.getBitmap(getContentResolver(),bedroomThreeFilePath);
                bedroomThreeImage.setImageBitmap(bedroomThreeBitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (requestCode==IMAGE_REQUEST_CODE7 && resultCode==RESULT_OK && data!=null && data.getData()!=null )
        {
            livingRoomFilePath=data.getData();
            //bitmap for storing the image
            try {

                livingRoomBitmap= MediaStore.Images.Media.getBitmap(getContentResolver(),livingRoomFilePath);
                livingRoomImage.setImageBitmap(livingRoomBitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (requestCode==IMAGE_REQUEST_CODE8 && resultCode==RESULT_OK && data!=null && data.getData()!=null )
        {
            swimmingPoolFilePath=data.getData();
            //bitmap for storing the image
            try {
                swimmingPoolBitmap= MediaStore.Images.Media.getBitmap(getContentResolver(),swimmingPoolFilePath);
                swimmingPoolImage.setImageBitmap(swimmingPoolBitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    //creating a method to get the absolute path of the image from the uri object

    public String getImagePath(Uri uri)
    {
        Cursor cursor=getContentResolver().query(uri,null,null,null,null);
        assert cursor != null;
        cursor.moveToFirst();
        String document_ID=cursor.getString(0);
        document_ID=document_ID.substring(document_ID.lastIndexOf(":")+1);
        cursor.close();

        cursor=getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,null,MediaStore.Images.Media._ID +"=?", new String[]{document_ID},null
        );
        assert cursor != null;
        cursor.moveToFirst();
        String path=cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();
        return path;
    }

    public String getImageMainBedroomPath(Uri uri1)
    {
        Cursor cursor1=getContentResolver().query(uri1,null,null,null,null);
        assert cursor1 != null;
        cursor1.moveToFirst();
        String document_ID=cursor1.getString(0);
        document_ID=document_ID.substring(document_ID.lastIndexOf(":")+1);
        cursor1.close();

        cursor1=getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,null,MediaStore.Images.Media._ID +"=?", new String[]{document_ID},null
        );
        assert cursor1 != null;
        cursor1.moveToFirst();
        String path=cursor1.getString(cursor1.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor1.close();
        return path;
    }

    public String getDiningRoomPath(Uri uri2)
    {
        Cursor cursor2=getContentResolver().query(uri2,null,null,null,null);
        assert cursor2 != null;
        cursor2.moveToFirst();
        String document_ID=cursor2.getString(0);
        document_ID=document_ID.substring(document_ID.lastIndexOf(":")+1);
        cursor2.close();

        cursor2=getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,null,MediaStore.Images.Media._ID +"=?", new String[]{document_ID},null
        );
        assert cursor2 != null;
        cursor2.moveToFirst();
        String path=cursor2.getString(cursor2.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor2.close();
        return path;
    }

    public String getKitchenPath(Uri uri3)
    {
        Cursor cursor3=getContentResolver().query(uri3,null,null,null,null);
        assert cursor3 != null;
        cursor3.moveToFirst();
        String document_ID=cursor3.getString(0);
        document_ID=document_ID.substring(document_ID.lastIndexOf(":")+1);
        cursor3.close();

        cursor3=getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,null,MediaStore.Images.Media._ID +"=?", new String[]{document_ID},null
        );
        assert cursor3 != null;
        cursor3.moveToFirst();
        String path=cursor3.getString(cursor3.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor3.close();
        return path;
    }

    public String getBedroomTwoPath(Uri uri4)
    {
        Cursor cursor4=getContentResolver().query(uri4,null,null,null,null);
        assert cursor4 != null;
        cursor4.moveToFirst();
        String document_ID=cursor4.getString(0);
        document_ID=document_ID.substring(document_ID.lastIndexOf(":")+1);
        cursor4.close();

        cursor4=getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,null,MediaStore.Images.Media._ID +"=?", new String[]{document_ID},null
        );
        assert cursor4 != null;
        cursor4.moveToFirst();
        String path=cursor4.getString(cursor4.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor4.close();
        return path;
    }

    public String getBedroomThreePath(Uri uri5)
    {
        Cursor cursor5=getContentResolver().query(uri5,null,null,null,null);
        assert cursor5 != null;
        cursor5.moveToFirst();
        String document_ID=cursor5.getString(0);
        document_ID=document_ID.substring(document_ID.lastIndexOf(":")+1);
        cursor5.close();

        cursor5=getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,null,MediaStore.Images.Media._ID +"=?", new String[]{document_ID},null
        );
        assert cursor5 != null;
        cursor5.moveToFirst();
        String path=cursor5.getString(cursor5.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor5.close();
        return path;
    }

    public String getLivingRoomPath(Uri uri6)
    {
        Cursor cursor6=getContentResolver().query(uri6,null,null,null,null);
        assert cursor6 != null;
        cursor6.moveToFirst();
        String document_ID=cursor6.getString(0);
        document_ID=document_ID.substring(document_ID.lastIndexOf(":")+1);
        cursor6.close();

        cursor6=getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,null,MediaStore.Images.Media._ID +"=?", new String[]{document_ID},null
        );
        assert cursor6 != null;
        cursor6.moveToFirst();
        String path=cursor6.getString(cursor6.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor6.close();
        return path;
    }

    public String getSwimmingPoolPath(Uri uri7)
    {
        Cursor cursor7=getContentResolver().query(uri7,null,null,null,null);
        assert cursor7 != null;
        cursor7.moveToFirst();
        String document_ID=cursor7.getString(0);
        document_ID=document_ID.substring(document_ID.lastIndexOf(":")+1);
        cursor7.close();

        cursor7=getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,null,MediaStore.Images.Media._ID +"=?", new String[]{document_ID},null
        );
        assert cursor7 != null;
        cursor7.moveToFirst();
        String path=cursor7.getString(cursor7.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor7.close();
        return path;
    }





    //a method to upload image to server
    public void uploadImages()
    {
        //getting the name of image
        String nameOfImage=mainHouseName.getText().toString();

        String imagePath=getImagePath(mainHouseFilePath);
        String nameOfMainBedroomImage=mainBedroomName.getText().toString();
        String mainBedroomImagePath=getImageMainBedroomPath(mainBedroomFilePath);
        //using upload server library
        try
        {
            String uploadID= UUID.randomUUID().toString();
            new MultipartUploadRequest(this,uploadID,url)
                   .addFileToUpload(imagePath,"image")
                   .addParameter("name",nameOfImage)
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload();

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void uploadMainBedroomImage()
    {
        //getting the name of image
        String nameOfMainBedroomImage=mainBedroomName.getText().toString();
        String mainBedroomImagePath=getImageMainBedroomPath(mainBedroomFilePath);

        //using upload server library
        try
        {
            String uploadID= UUID.randomUUID().toString();
            new MultipartUploadRequest(this,uploadID,url)
                    .addFileToUpload(mainBedroomImagePath,"image")
                    .addParameter("name",nameOfMainBedroomImage)
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload();

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void uploadDiningRoomImage()
    {
        //getting the name of image
        String nameOfDiningRoom=diningRoomName.getText().toString();
        String diningRoomImagePath=getDiningRoomPath(diningRoomFilePath);

        //using upload server library
        try
        {
            String uploadID= UUID.randomUUID().toString();
            new MultipartUploadRequest(this,uploadID,url)
                    .addFileToUpload(diningRoomImagePath,"image")
                    .addParameter("name",nameOfDiningRoom)
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void uploadKitchenImage()
    {
        //getting the name of image
        String nameOfKitchen=kitchenName.getText().toString();
        String kitchenImagePath=getKitchenPath(kitchenFilePath);

        //using upload server library
        try
        {
            String uploadID= UUID.randomUUID().toString();
            new MultipartUploadRequest(this,uploadID,url)
                    .addFileToUpload(kitchenImagePath,"image")
                    .addParameter("name",nameOfKitchen)
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void uploadBedroomTwoImage()
    {
        //getting the name of image
        String nameOfBedroomTwo=bedroomTwoName.getText().toString();
        String bedroomTwoImagePath=getBedroomTwoPath(bedroomTwoFilePath);

        //using upload server library
        try
        {
            String uploadID= UUID.randomUUID().toString();
            new MultipartUploadRequest(this,uploadID,url)
                    .addFileToUpload(bedroomTwoImagePath,"image")
                    .addParameter("name",nameOfBedroomTwo)
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void uploadBedroomThreeImage()
    {
        //getting the name of image
        String nameOfBedroomThree=bedroomThreeName.getText().toString();
        String bedroomThreeImagePath=getBedroomThreePath(bedroomThreeFilePath);

        //using upload server library
        try
        {
            String uploadID= UUID.randomUUID().toString();
            new MultipartUploadRequest(this,uploadID,url)
                    .addFileToUpload(bedroomThreeImagePath,"image")
                    .addParameter("name",nameOfBedroomThree)
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void uploadLivingRoomImage()
    {
        //getting the name of image
        String nameOfLivingRoom=livingRoomName.getText().toString();
        String livingRoomImagePath=getLivingRoomPath(livingRoomFilePath);

        //using upload server library
        try
        {
            String uploadID= UUID.randomUUID().toString();
            new MultipartUploadRequest(this,uploadID,url)
                    .addFileToUpload(livingRoomImagePath,"image")
                    .addParameter("name",nameOfLivingRoom)
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void uploadSwimmingPoolImage()
    {
        //getting the name of image
        String nameOfSwimmingPool=swimmingPoolName.getText().toString();
        String swimmingPoolImagePath=getSwimmingPoolPath(swimmingPoolFilePath);

        //using upload server library
        try
        {
            String uploadID= UUID.randomUUID().toString();
            new MultipartUploadRequest(this,uploadID,url)
                    .addFileToUpload(swimmingPoolImagePath,"image")
                    .addParameter("name",nameOfSwimmingPool)
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    @SuppressLint("StaticFieldLeak")
    private class UploadData extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... voids) {
            try {
                URL url=new URL(address);
                HttpURLConnection httpURLConnection=(HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);

                //opening an output stream
                OutputStream outputStream=httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data_to_post= URLEncoder.encode("location","UTF-8")+"="+URLEncoder.encode(locationString,"UTF-8")+"&"
                        + URLEncoder.encode("description","UTF-8")+"="+URLEncoder.encode(descriptionString,"UTF-8")+"&"
                        + URLEncoder.encode("rent","UTF-8")+"="+URLEncoder.encode(rentString,"UTF-8")+"&"
                        + URLEncoder.encode("numberOfBedrooms","UTF-8")+"="+URLEncoder.encode(bedroomString,"UTF-8");
                bufferedWriter.write(data_to_post);

                //closing open objects
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                Log.i("Data","Message"+data_to_post);

                InputStream inputStream=httpURLConnection.getInputStream();
                BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String line;
                String result="";

                while((line=bufferedReader.readLine())!=null)
                {
                    result+=line;
                }

                //closing open connections
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            alertDialog=new AlertDialog.Builder(com.fixo.advertiser.TrialImageUpload.this);
            alertDialog.setTitle("Login status");

        }

        @Override
        protected void onPostExecute(String result) {
           String value=result;
            alertDialog.setMessage(result);

            try {

                if (value.contains("sent")) {

                    alertDialog.show();
                }
            }
            catch (NullPointerException e)
            {
                Toast.makeText(com.fixo.advertiser.TrialImageUpload.this,"No network connection",Toast.LENGTH_LONG).show();
            }
        }
    }


}
