package com.fixo.advertiser;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

/**
 * Created by FIXO on 11/08/2018.
 */

public class TrialMoreDetails extends AppCompatActivity {

    private TextView title1,title2;
    private ImageView imageView1,getImageView2;
    String title1String,title2String, imageView1String,imageView2String;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.more_details_trial);
        title1=findViewById(R.id.title);
        title2=findViewById(R.id.title2);
        imageView1=findViewById(R.id.imageView1);
        getImageView2=findViewById(R.id.imageView2);

        title1String=getIntent().getStringExtra("title");
        title2String=getIntent().getStringExtra("title");
        imageView1String=getIntent().getStringExtra("image1");
        imageView2String=getIntent().getStringExtra("image2");


        title1.setText(title1String);
        title2.setText(title2String);
        Glide.with(this).load(imageView1String).into(imageView1);
        Glide.with(this).load(imageView2String).into(getImageView2);


    }
}
