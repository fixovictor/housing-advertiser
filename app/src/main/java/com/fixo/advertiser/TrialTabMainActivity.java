package com.fixo.advertiser;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;


/**
 * Created by FIXO on 16/08/2018.
 */

public class TrialTabMainActivity extends AppCompatActivity{
    DrawerLayout drawerLayout;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.drawer_layout);
            drawerLayout=findViewById(R.id.drawerLayout);
            Toolbar toolbar=findViewById(R.id.toolBar);
            setSupportActionBar(toolbar);
            ViewPager pager=findViewById(R.id.viewPager);

            TabAdapter adapter=new TabAdapter(getSupportFragmentManager());
            pager.setAdapter(adapter);

            ActionBarDrawerToggle toggle=new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open_drawer,R.string.close_drawer);
            drawerLayout.setDrawerListener(toggle);
            toggle.syncState();
           // NavigationView navigationView=findViewById(R.id.);
           // navigationView.setNavigationItemSelectedListener(this);




        }
}
