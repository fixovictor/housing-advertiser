package com.fixo.advertiser;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by FIXO on 25/07/2018.
 */

public class UserRegistration extends AppCompatActivity {

    private EditText username,password,email;
    private String name,userPassword,userEmail;
    AlertDialog alertDialog;
    private Button submit;
    private  String address="http://192.168.44.112/android/registration.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);
        username=findViewById(R.id.userRegistrationName);
        password=findViewById(R.id.userRegistrationPassword);
        email=findViewById(R.id.userRegistrationEmail);
        submit=findViewById(R.id.userRegistrationButton);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name=username.getText().toString();
                userPassword=password.getText().toString();
                userEmail=email.getText().toString();
                RegistrationInBackgroundTask rg=new RegistrationInBackgroundTask();
                rg.execute(name,userEmail,userPassword);

            }
        });

    }


    @SuppressLint("StaticFieldLeak")
    private class RegistrationInBackgroundTask extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... voids) {
            try {
                URL url=new URL(address);
                HttpURLConnection httpURLConnection=(HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);

                //opening an output stream
                OutputStream outputStream=httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                String data_to_post= URLEncoder.encode("name","UTF-8")+"="+URLEncoder.encode(name,"UTF-8")+"&"
                        + URLEncoder.encode("email","UTF-8")+"="+URLEncoder.encode(userEmail,"UTF-8")+"&"
                        + URLEncoder.encode("password","UTF-8")+"="+URLEncoder.encode(userPassword,"UTF-8");
                bufferedWriter.write(data_to_post);

                //closing open objects
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();

                Log.i("Data","Message"+data_to_post);

                InputStream inputStream=httpURLConnection.getInputStream();
                BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                String line;
                String result="";

                while((line=bufferedReader.readLine())!=null)
                {
                    result+=line;
                }

                //closing open connections
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            alertDialog=new AlertDialog.Builder(com.fixo.advertiser.UserRegistration.this).create();

        }

        @Override
        protected void onPostExecute(String result) {

            alertDialog.setMessage(result);
            alertDialog.show();

        }
    }
}
